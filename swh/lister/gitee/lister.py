# Copyright (C) 2021-2022  The Software Heritage developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import logging
from typing import Any, Dict, Iterator, List

from swh.lister.pattern import CredentialsType, StatelessLister
from swh.scheduler.interface import SchedulerInterface
from swh.scheduler.model import ListedOrigin

logger = logging.getLogger(__name__)

# Aliasing the page results returned by `get_pages` method from the lister.
repositories = List[Dict[str, Any]]


# If there is no need to keep state, subclass StatelessLister[NewForgeListerPage]
class GiteeLister(StatelessLister[repositories]):
    """List origins from the "gitee" forge."""

    # Part of the lister API, that identifies this lister
    LISTER_NAME = "gitee"
    # (Optional) CVS type of the origins listed by this lister, if constant
    VISIT_TYPE = "git"

    # Instance URLs include the hostname and the common path prefix of processed URLs
    BASE_URL = "https://gitee.com"

    def __init__(
        self,
        # Required
        scheduler: SchedulerInterface,
        # Required whether lister supports authentication or not
        credentials: CredentialsType = None,
    ):
        super().__init__(
            scheduler=scheduler, credentials=credentials, url=self.BASE_URL
        )

        self.session.headers.update({"Accept": "application/html"})

    def get_pages(self) -> Iterator[List[Dict[str, Any]]]:
        page_results: List[Dict[str, Any]] = [{}]
        # TODO
        # has to be in the form {url:...,last_update_interval:...}
        yield page_results

    # TODO
    def get_origins_from_page(
        self, page: List[Dict[str, Any]]
    ) -> Iterator[ListedOrigin]:
        """Convert a page of GiteeLister repositories into a list of ListedOrigins"""
        assert self.lister_obj.id is not None

        for element in page:
            yield ListedOrigin(
                lister_id=self.lister_obj.id,
                visit_type=self.VISIT_TYPE,
                url=element["url"],
                last_update=None,
            )
