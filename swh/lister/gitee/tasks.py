# Copyright (C) 2023 The Software Heritage developers
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

from typing import Dict

from celery import shared_task

from .lister import GiteeLister


@shared_task(name=f"{__name__}.GiteeListerTask")
def list_gitee(**lister_args) -> Dict[str, str]:
    """Lister task for repo.or.cz"""
    lister = GiteeLister.from_configfile(**lister_args)
    return lister.run().dict()
